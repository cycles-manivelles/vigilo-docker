<?php

declare (strict_types = 1);

require "./fake.php";
require "../../src/user/user_model.php";
use PHPUnit\Framework\TestCase;

final class UserModelTest extends TestCase
{

    private function _serializeUser($user)
    {
        return array_values((array) $user);
    }

    public function test_GetUsers_return_the_three_users_registred(): void
    {
        # arrange
        $expectedUsers = [fakeUser(1), fakeUser(2), fakeUser(3)];
        $serializedUsers = [$this->_serializeUser(fakeUser(1)),
            $this->_serializeUser(fakeUser(2)), $this->_serializeUser(fakeUser(3))];

        # mock
        $mockQueryResult = $this->createMock(mysqli_result::class);
        $mockQueryResult->method("fetch_all")
            ->will($this->returnValue($serializedUsers));

        $map = [
            ["DESCRIBE `obs_roles`", null, true],
            ["SELECT * FROM obs_roles", null, $mockQueryResult],
        ];

        $mockDb = $this->createMock(mysqli::class);
        $mockDb->method('query')
            ->will($this->returnValueMap($map));

        # act
        $userModel = new UserModel($mockDb);
        $result = $userModel->GetUsers();

        # assert
        $this->assertEquals(
            $result,
            $expectedUsers
        );
    }

    public function test_NumberOfUsers_return_3_when_three_users_registred(): void
    {
        # arrange
        $expected = 3;
        $serializedUsers = [$this->_serializeUser(fakeUser(1)),
            $this->_serializeUser(fakeUser(2)), $this->_serializeUser(fakeUser(3))];

        # mock
        $mockQueryResult = $this->createMock(mysqli_result::class);
        $mockQueryResult = new MysqliMock($mockQueryResult, 3);

        $map = [
            ["DESCRIBE `obs_roles`", null, true],
            ["SELECT * FROM obs_roles", null, $mockQueryResult],
        ];

        $mockDb = $this->createMock(mysqli::class);
        $mockDb->method('query')
            ->will($this->returnValueMap($map));

        # act
        $userModel = new UserModel($mockDb);
        $result = $userModel->NumberOfUsers();

        # assert
        $this->assertEquals(3, $mockQueryResult->num_rows);
        $this->assertEquals(
            $result,
            $expected
        );
    }

    public function test_constructor_raise_if_table_not_exist(): void
    {
        # arrange

        # mock
        $mockQueryResult = $this->createMock(mysqli_result::class);

        $map = [["DESCRIBE `obs_roles`", null, false]];

        $mockDb = $this->createMock(mysqli::class);
        $mockDb->method('query')
            ->will($this->returnValueMap($map));

        # assert
        $this->expectException(PDOException::class);
        $this->expectExceptionMessage("Table obs_roles missing");
        # act
        $userModel = new UserModel($mockDb);

    }

}
