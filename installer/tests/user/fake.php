<?php

require "../../src/user/user.php";

function fakeUser($id = 1, $moderationKey = "645mlk",
    $role = "admin", $name = "userName", $login = "userLogin",
    $password = "userpass", $city = "") {
    return new User($id, $moderationKey, $role, $name, $login, $password, $city);
}

class MysqliMock
{
    public $mysqli;
    public $num_rows;

    public function __construct($mysqli, $num_rows)
    {
        $this->mysqli = $mysqli;
        $this->num_rows = $num_rows;
    }
    public function __call($method, $args)
    {
        call_user_func_array(array($this->mysqli, "method"), $args);

    }

    public function __get($name)
    {
        //you can mock the properties here
    }

    public function __set($name, $value)
    {

    }
}
