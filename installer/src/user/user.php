<?php

final class User
{
    private $_Id;
    private $_ModerationKey;
    private $_Role;
    private $_Name;
    private $_Login;
    private $_Password;
    private $_City;

    public function __construct(
        int $Id,
        string $ModerationKey,
        string $Role,
        string $Name,
        string $Login,
        string $Password,
        string $City
    ) {
        $this->_Id = $Id;
        $this->_ModerationKey = $ModerationKey;
        $this->_Role = $Role;
        $this->_Name = $Name;
        $this->_Login = $Login;
        $this->_Password = $Password;
        $this->_City = $City;
    }

    public function Id(): int
    {
        return $this->_Id;
    }
    public function ModerationKey(): string
    {
        return $this->_ModerationKey;
    }
    public function Role(): string
    {
        return $this->_Role;
    }
    public function Name(): string
    {
        return $this->_Name;
    }
    public function Login(): string
    {
        return $this->_Login;
    }
    public function Password(): string
    {
        return $this->_Password;
    }
    public function City(): string
    {
        return $this->_City;
    }
}
