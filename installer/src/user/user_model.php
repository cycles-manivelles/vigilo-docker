<?php

class UserModel
{
    private $_mysqli;
    public function __construct(mysqli $mysqli)
    {
        $this->_mysqli = $mysqli;
        if (!$this->_mysqli->query("DESCRIBE `obs_roles`")) {
            throw new PDOException("Table obs_roles missing");
        }
    }

    public function GetUsers()
    {
        $usersQueryResult = $this->_mysqli->query("SELECT * FROM obs_roles");
        $users = array();
        foreach ($usersQueryResult->fetch_all() as $index => $user) {
            $users[$index] = new User($user[0], $user[1], $user[2], $user[3], $user[4], $user[5], $user[6]);
        }
        return $users;

    }

    public function NumberOfUsers()
    {
        $usersQueryResult = $this->_mysqli->query("SELECT * FROM obs_roles");
        return $usersQueryResult->num_rows;

    }
}
