<?php

$config['MYSQL_HOST'] = getenv('MYSQL_HOST');
$config['MYSQL_USER'] = getenv('MYSQL_USER');
$config['MYSQL_PASSWORD'] = getenv('MYSQL_PASSWORD');
$config['MYSQL_DATABASE'] = getenv('MYSQL_DATABASE');
$user["name"] =  getenv('VIGILO_ADMIN_USER_NAME');
$user["login"] =  getenv('VIGILO_ADMIN_LOGIN');
$user["password"] =  getenv('VIGILO_ADMIN_PASSWORD');

# On attend que la base soit up and ready
$max_loop = 10;
while ((!mysqli_connect(
        $config['MYSQL_HOST'],
        $config['MYSQL_USER'],
        $config['MYSQL_PASSWORD'],
        $config['MYSQL_DATABASE']
    )) && ($max_loop > 0)
) {
    $max_loop--;
    sleep(1);
}


# on voit si on se connecte bien à la base finalement
if (!$db = mysqli_connect(
    $config['MYSQL_HOST'],
    $config['MYSQL_USER'],
    $config['MYSQL_PASSWORD'],
    $config['MYSQL_DATABASE']
)) {
    throw new RuntimeException("could not connect to the database");
}

# on regarde s'il y a un utilisateur et les tables pour s'arrêter là ou non
if (mysqli_query($db, "DESCRIBE `obs_roles`")) {
    $query_installed = mysqli_query($db, "SELECT * FROM obs_roles");
    fwrite(STDOUT, $query_installed);
    if (mysqli_num_rows($query_installed) != 0) {
        fwrite(STDOUT, "already installed !");
        exit(0);
    }
}

# on créé les schémas
$sql_scripts_path = "/installer/sql-scripts/";
foreach (array_diff(scandir($sql_scripts_path), array('..', '.')) as $sql_script_name) {
    $sql_content = file("{$sql_scripts_path}{$sql_script_name}");

    fwrite(STDOUT, "\n{$sql_script_name} to be loaded !\n");

    $query = '';
    foreach ($sql_content as $line) {

        $startWith = substr(trim($line), 0, 2);
        $endWith = substr(trim($line), -1, 1);

        if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
            continue;
        }

        $query = $query . $line;
        if ($endWith == ';') {
            mysqli_query($db, $query) or exit(1);
            $query = '';
        }
    }

    fwrite(STDOUT, "{$sql_script_name} loaded !\n");
}

# on créé l'utilisateur admin
if (
    empty($user["password"]) ||
    empty($user["name"]) ||
    empty($user["login"])
) {
    throw new InvalidArgumentException("missing credentials");
}

$password = hash('sha256', $user["password"]);
fwrite(STDOUT, "\ngo create admin user\n");
mysqli_query($db, "INSERT INTO obs_roles (role_key,
                                            role_name,
                                            role_owner,
                                            role_login,
                                            role_password,
                                            role_city)
                             VALUES ('',
                                      'admin',
                                      '" . $user["name"] . "',
                                      '" . $user["login"] . "',
                                      '" . $password . "',
                                      '')") or exit(1);