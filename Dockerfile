FROM alpine/git as builder

ARG VIGILO_VERSION=0.0.16

RUN git clone https://github.com/jesuisundesdeux/vigilo-backend.git -b $VIGILO_VERSION --single-branch /target/vigilo

FROM php:7.2-apache

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev

# Activate php extensions
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd mysqli exif

# Enable Remote IP and Rewrite
RUN a2enmod remoteip && \
    a2enmod rewrite

# Add logs with good ip
COPY --from=builder --chown=www-data:www-data /target/vigilo/docker_images/web/remoteip.conf /etc/apache2/conf-enabled

# Add default Apache conf
COPY --from=builder --chown=www-data:www-data /target/vigilo/docker_images/web/000-default.conf /etc/apache2/sites-enabled/000-default.conf

# Activate php log
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

# Add app
COPY --from=builder --chown=www-data:www-data /target/vigilo/app /var/www/html
COPY --from=builder --chown=www-data:www-data /target/vigilo/config/config.php.docker /var/www/html/config/config.php

# Add installer and sql for init
COPY installer /installer
COPY --from=builder /target/vigilo/mysql/init /installer/sql-scripts

COPY entrypoint.sh /entrypoint.sh
COPY Dockerfile /Dockerfile

ENTRYPOINT /entrypoint.sh